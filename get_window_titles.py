import win32gui
import win32process
import psutil


# Function to grab window title from process main window
def get_window_titles(process_name):
    window_titles = {}
    for proc in psutil.process_iter():
        if proc.name() == process_name:
            pid = proc.pid
            window_titles[pid] = []

            # Actual title grabbing happens in here
            def titlegrab(hwnd, _):
                if win32gui.IsWindowVisible(hwnd):
                    _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
                    if pid == found_pid:
                        title = win32gui.GetWindowText(hwnd)
                        if title:
                            window_titles[pid].append(title)
                return True

            win32gui.EnumWindows(titlegrab, None)
    return window_titles


# Printing list of process ids and titles
titles = get_window_titles('')  # Swap .exe to your process
for pid, window_list in titles.items():
    for title in window_list:
        print(f"{pid:8}--{title:->30}")
