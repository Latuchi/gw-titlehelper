import cv2
import keyboard
import numpy as np
import psutil
import pyautogui
import subprocess
import sys
import time
import win32api
import win32con
import win32gui
import win32process


# Function to grab window title from process main window
def get_window_titles(process_name):
    window_titles = {}
    for proc in psutil.process_iter():
        if proc.name() == process_name:
            pid = proc.pid
            window_titles[pid] = []

            # Actual title grabbing happens in here
            def titlegrab(hwnd, _):
                if win32gui.IsWindowVisible(hwnd):
                    _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
                    if pid == found_pid:
                        title = win32gui.GetWindowText(hwnd)
                        if title:
                            window_titles[pid].append(title)
                return True

            win32gui.EnumWindows(titlegrab, None)
    return window_titles


# Printing list of process ids and titles
titles = get_window_titles('Gw.exe')  # Swap .exe to your process
print(f"{'PID':>8}--{'Window title':->30}")
print(f"{'':>8}{'':>30}")  # Overcomplicated empty line
for pid, window_list in titles.items():
    for title in window_list:
        print(f"{pid:8}--{title:->30}")

# time.sleep(3)
# Screenshots the inventory for no reason. I just tested how this works
iml = pyautogui.screenshot(region=(2842, 1213, 389, 88))
iml.save(r"./inv.png")
point = pyautogui.locateOnScreen("./point.png", confidence=0.4)
quti = "Press B to exit the script"
print(point)
print(quti)


# Function for double clicking
def dclick():
    r2 = np.random.uniform(0.02, 0.06)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    time.sleep(r2)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    time.sleep(r2)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    time.sleep(r2)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    pass


while point:
    # print(point)  # uncomment this line if you want to see if there are any detections
    cntr = pyautogui.center(
        point)  # Storing coordinates of the center of detectable object
    pyautogui.moveTo(cntr)  # Moving cursor to center of detected object
    dclick()  # Double clicking the object
    r1 = np.random.uniform(
        5.08, 5.25)  # Randomizing the delay between scan/double click
    time.sleep(r1)
    iml = pyautogui.screenshot(
        region=(2842, 1213, 389,
                88))  # Screenshots the inventory region for no reason again
    iml.save(r"./inv.png")  # Overwriting the earlier screenshot
    if keyboard.is_pressed('B'):
        sys.exit()
    point = pyautogui.locateOnScreen(
        "./point.png",
        confidence=0.66)  # Updating the scan what happens on screen
